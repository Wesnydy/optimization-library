#include <fstream>
#include <iostream>

#include "actvity_selection_05.hpp"
#include "binary_knapsack_03.hpp"
#include "edit_distance_10.hpp"
#include "fractional_knapsack_06.hpp"
#include "longest_common_subsequence_07.hpp"
#include "simulated_annealing.hpp"

using namespace std;

void run_activity_selection(){
//	Atividade ativ[] = {
//		new Atividade(1,2),
//		new Atividade(3,4),
//		new Atividade(0,6),
//		new Atividade(5,7),
//		new Atividade(8,9),
//		new Atividade(5,9)
//	}

Atividade ativ[] = {
		Atividade(1,2),
		Atividade(3,4),
		Atividade(0,6),
		Atividade(5,7),
		Atividade(8,9),
		Atividade(5,9)
	};

	cout << "Conjunto de atividades inicial: \n";
	for(int i=0; i<6; i++){
		cout << "inicio: " << ativ[i].ini << "fim: " << ativ[i].fim << "\n";
	}

	vector<Atividade> resultado = selecionar_atividades(ativ, 6);

	cout << "As seguintes atividades foram selecionadas: \n";

	for(unsigned int i=0; i<resultado.size(); i++){
		cout << "inicio: " << resultado.at(i).ini << "fim: " << resultado.at(i).fim << "\n";
	}

}


void run_binary_knapsack(){
	int peso[] = {10, 20, 30};
	int valores[] = {60, 100, 120};
    int capacidade = 50;
    int itens = 3;


    cout << "Objetos: \n";

    for(int i=0; i<3; i++){
    	cout << "Peso: " << peso[i] << "valor: " << valores[i] << "\n";
    }

    cout << "Capacidade da mochila: 50 \n";

    int resultado = binary_knapsack(capacidade, itens, peso, valores);

    cout << "O maior valor que pode ser armazenado na mochila é: " << resultado << "\n";
}

void run_edit_distance(){
	string str1 ("sunday");
	string str2 ("saturday");

	cout << "Strings submetidas: \n";
	cout << str1 << "\n";
	cout << str2 << "\n";

	int resultado = edit_distance(str1, str2, str1.length(), str2.length());

	cout << "O número minimo de edições é: " << resultado <<"\n";
}

void run_fractional_knapsack(){
    Item entrada[] = {
    	Item(60, 10),
    	Item(100, 20),
    	Item(120, 30)
    };
    int capacidade = 50;
    int quantidade = 3;

    for(int i=0; i<quantidade; i++){
    	cout << "Peso: " << entrada[i].peso << "valor: " << entrada[i].valor << "\n";
    }

    double resultado =  fractional_knapsack(capacidade, quantidade, entrada);

    cout << "O maior valor que pode ser armazenado na mochila é: " << resultado << "\n";
}

void run_longest_common_subsequence(){
	string str1 = "ABCDGH";
	string str2 = "AEDFHR";

	cout << "Strings submetidas: \n";
	cout << str1 << "\n";
	cout << str2 << "\n";

	int resultado = longest_common_subsequence(str1, str2, str1.length(), str2.length());

	cout << "O tamanho da maior subsequencia comum é:" << resultado << " \n";
}

void run_simulated_annealing(string fileN){
	const string IN_PATH = "src/bpp-sa/instances/bpp" + fileN + ".BPP";
	const string OUT_PATH = "src/bpp-sa/log.log";
	
	ifstream inFile(IN_PATH.c_str(), ios::in);
	int value;
	inFile >> value;
	int size;
	inFile >> size;
	
	vector<int> items;
	
	for (int i = 0; inFile >> value; i++) {
        items.push_back(value);
    }

    int result = simulatedAnnealing(size, items);
    
    ofstream outFile;
    
    outFile.open(OUT_PATH, ios::app);
    outFile <<"Input: " << IN_PATH << endl;
    outFile <<"Bins: " << result << endl;
    outFile << "-----------------------" << endl << endl;
    
    outFile.close();
    
    
}


int main(int argc, char **argv){

	if (argc < 2) {
		printf("Parâmetros Insuficientes");
		return 1;
	}

	switch(atoi(argv[1])){
		case 1: {
			run_activity_selection();
			break;
		}
		case 2: {
			run_binary_knapsack();
			break;
		}
		case 3: {
			run_edit_distance();
			break;
		}
		case 4: {
			run_fractional_knapsack();
			break;
		}
		case 5: {
			run_longest_common_subsequence();
			break;
		}
		case 6: {
			string str(argv[2]);
			run_simulated_annealing(str);
			break;
		}
	}

	return 0;
}
