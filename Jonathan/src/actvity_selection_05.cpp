#include "actvity_selection_05.hpp"


bool cmp(struct Atividade a, struct Atividade b){
  return a.fim < b.fim;
}


vector<Atividade> selecionar_atividades(struct Atividade ativ[], int ativ_size){
  sort(ativ, &ativ[ativ_size], cmp);

  vector<Atividade> resultado;
  resultado.push_back(ativ[0]);

  for(int i=1; i<ativ_size; i++){
    if (ativ[i].ini >= resultado.back().fim){
      resultado.push_back(ativ[i]);
    }
  }

  return resultado;
}

