#include "longest_common_subsequence_07.hpp"

int longest_common_subsequence(string str1, string str2, int size1, int size2){
  int tabela[size1+1][size2+1];

  for (int i=0; i<=size1; i++){
    for (int j=0; j<=size2; j++){
      if (j == 0 || i == 0)
        tabela[i][j] = 0;
      else if (str1[i-1] == str2[j-1])
        tabela[i][j] = tabela[i-1][j-1] + 1;
      else
        tabela[i][j] = max(tabela[i-1][j], tabela[i][j-1]);
    }
  }

  return tabela[size1][size2];
}
