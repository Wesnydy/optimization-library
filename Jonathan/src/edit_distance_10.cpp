#include "edit_distance_10.hpp"


int edit_distance(string str1, string str2, int size1, int size2){

	int tabela[size1+1][size2+1];

	for(int i=0; i<=size1; i++){
		for(int j=0; j<=size2; j++){
			if(i == 0)
				tabela[i][j] = j;
			else if(j == 0)
				tabela[i][j] = i;
			else if(str1[i-1] == str2[j-1])
				tabela[i][j] = tabela[i-1][j-1];
			else
				tabela[i][j] = 1 + min(min(tabela[i][j-1], tabela[i-1][j]), tabela[i-1][j-1]);
		}
	}

	return tabela[size1][size2];
}
