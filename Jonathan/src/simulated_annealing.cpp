#include "simulated_annealing.hpp"


double acceptanceChance(int energy, int newEnergy, double temperature){
  if(newEnergy < energy) return 1;

  return exp((energy-newEnergy)/temperature);
}

int bins(int binSize, vector<int> items){
  int bins = 0;
  int curBinItems = 0;

  for(int i=0; i<items.size(); i++){
    // if a bin has just been initialized, increase the amount of bins
    if(curBinItems == 0) bins++;

    //if the current item fits in the current bin, add it to the bin
    if(curBinItems + items.at(i) <= binSize){
      curBinItems += items.at(i);
    }else{
      //if the current item doesn't fit the bin, initialize a new one, and then add the item
      curBinItems = items.at(i);
      bins++;
    }

    //if the bin is full, initialize a new one
    if(curBinItems == binSize) curBinItems = 0;
  }

  return bins;
}

int simulatedAnnealing(int binSize, vector<int> items){
  double temperature = DBL_MAX;
  double coolingRate = 0.001;

  vector<int> currentSolution(items);

  cout << "Quantidade inicial de cestos: " << bins(binSize, currentSolution) << "\n";

  vector<int> bestSolution = currentSolution;
  int bestEnergy = bins(binSize, bestSolution);

  while(temperature > 1){
    vector<int> newSolution(currentSolution);

    int pos1 = rand() % newSolution.size();
    int pos2 = rand() % newSolution.size();

    swap(newSolution[pos1],newSolution[pos2]);

    int currentEnergy = bins(binSize, currentSolution);
    int neighbourEnergy = bins(binSize, newSolution);

    if(acceptanceChance(currentEnergy, neighbourEnergy, temperature) > ((double) rand() / (RAND_MAX))){
      currentSolution = newSolution;
      int currentEnergy = neighbourEnergy;
    }

    if(currentEnergy < bestEnergy){
      bestSolution = currentSolution;
    }

    temperature *= 1 - coolingRate;
  }

  cout << "Quantidade final de cestos: " << bins(binSize, bestSolution) << "\n";

  return bins(binSize, bestSolution);
}
