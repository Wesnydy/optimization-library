#include "fractional_knapsack_06.hpp"


bool cmp(struct Item a, struct Item b){
  return (double)a.valor/a.peso > (double)b.valor/b.peso;
}


double fractional_knapsack(int capacidade, int numero_de_itens, struct Item itens[]){
  int peso_na_bolsa = 0;
  double resultado = 0;

  sort(itens,&itens[numero_de_itens], cmp);

  for(int i=0; i<numero_de_itens; i++){
    if(peso_na_bolsa + itens[i].peso <= capacidade){
      peso_na_bolsa += itens[i].peso;
      resultado += itens[i].valor;
    }else{
      resultado += itens[i].valor * ((double)(capacidade - peso_na_bolsa)/itens[i].peso);
      break;
    }
  }

  return resultado;
}
