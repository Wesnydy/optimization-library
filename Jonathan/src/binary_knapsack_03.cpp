#include "binary_knapsack_03.hpp"


int binary_knapsack(int capacidade, int itens, int peso[], int valor[]){
   int tabela[itens+1][capacidade+1];

   for (int i = 0; i <= itens; i++){
       for (int j = 0; j <= capacidade; j++){
           if (i==0 || j==0)
               tabela[i][j] = 0;
           else if (peso[i-1] <= j)
                 tabela[i][j] = max(valor[i-1] + tabela[i-1][j-peso[i-1]],  tabela[i-1][j]);
           else
                 tabela[i][j] = tabela[i-1][j];
       }
   }

   return tabela[itens][capacidade];
}
