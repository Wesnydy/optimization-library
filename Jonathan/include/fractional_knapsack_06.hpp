#ifndef _FRACTIONAL_KNAPSACK_H_
#define _FRACTIONAL_KNAPSACK_H_

#include <iostream>
#include <algorithm>

using namespace std;

struct Item{
  int peso;
  int valor;

  Item(int peso, int valor) : peso(peso), valor(valor){}
};


bool cmp(struct Item a, struct Item b);


double fractional_knapsack(int capacidade, int numero_de_itens, struct Item itens[]);

#endif // _FRACTIONAL_KNAPSACK_H_
