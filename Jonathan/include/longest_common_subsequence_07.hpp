#ifndef _LONGEST_COMMON_SUBSEQUENCE_H_
#define _LONGEST_COMMON_SUBSEQUENCE_H_


#include <iostream>
using namespace std;


int longest_common_subsequence(string str1, string str2, int size1, int size2);

#endif // _LONGEST_COMMON_SUBSEQUENCE_H_