#ifndef _EDIT_DISTANCE_H_
#define _EDIT_DISTANCE_H_

#include <iostream>
using namespace std;


int edit_distance(string str1, string str2, int size1, int size2);

#endif // _EDIT_DISTANCE_H_