#ifndef _ACTIVITY_SELECTION_H_
#define _ACTIVITY_SELECTION_H_


#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct Atividade{
  int ini;
  int fim;

  Atividade(int ini, int fim) : ini(ini), fim(fim){}
};

bool cmp(struct Atividade a, struct Atividade b);

vector<Atividade> selecionar_atividades(struct Atividade ativ[], int ativ_size);

#endif // _ACTIVITY_SELECTION_H_