#ifndef _SIMULATED_ANNEALING_H_
#define _SIMULATED_ANNEALING_H_

#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <float.h>

using namespace std;

double acceptanceChance(int energy, int newEnergy, double temperature);

int bins(int binSize, vector<int> items);

int simulatedAnnealing(int binSize, vector<int> items);


#endif // _SIMULATED_ANNEALING_H_
