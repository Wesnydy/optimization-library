#!/bin/sh

CBIN=/Claudiomar/lib
CLOG=log/claudiomar.log

JBIN=/Jonathan/lib
JLOG=log/jonathan.log

WBIN=/Wesnydy/lib
WLOG=log/wesnydy.log

echo "### Executando testes para os algoritmos de Claudiomar ###\n" | tee $CLOG

echo "# Executando o algoritmo PascalTriangle...\n" | tee -a $CLOG
./$CBIN 1 1 >> $CLOG

echo "# Executando o algoritmo CoinChange...\n" | tee -a $CLOG
./$CBIN 1 2 >> $CLOG

echo "# Executando o algoritmo BoxStacking...\n" | tee -a $CLOG
./$CBIN 1 3 >> $CLOG

echo "# Executando o algoritmo DijkstraShortestPath...\n" | tee -a $CLOG
./$CBIN 1 4 >> $CLOG

echo "# Executando o algoritmo KruskalMst...\n" | tee -a $CLOG
./$CBIN 1 5 >> $CLOG

echo "# Executando o algoritmo BPP_GA...\n" | tee -a $CLOG
./$CBIN 1 6 >> $CLOG

echo "### Executando testes para os algoritmos de Jonathan ###\n" | tee $JLOG

echo "# Executando o algoritmo ActivitySelection...\n" | tee -a $JLOG
./$JBIN 1 >> $JLOG

echo "# Executando o algoritmo BinaryKnapsack...\n" | tee -a $JLOG
./$JBIN 2 >> $JLOG

echo "# Executando o algoritmo EditDistance...\n" | tee -a $JLOG
./$JBIN 3 >> $JLOG

echo "# Executando o algoritmo FractionalKnapsack...\n" | tee -a $JLOG
./$JBIN 4 >> $JLOG

echo "# Executando o algoritmo LongestCommonSubsequence...\n" | tee -a $JLOG
./$JBIN 5 >> $JLOG

echo "### Executando testes para os algoritmos de Wesnydy ###\n" | tee $WLOG

echo "# Executando o algoritmo WordBreak...\n" | tee -a $WLOG

counter=1
for i in Wesnydy/testfiles/wordbreak/*."dict.in";
    do
        echo "Teste $counter\n" >> $WLOG
        ./$WBIN 1 $i Wesnydy/testfiles/wordbreak/$counter.in >> $WLOG
        echo >> $WLOG
        counter=$((counter+1))
    done

echo "# Executando o algoritmo SubsetSum...\n" | tee -a $WLOG

counter=1
for i in Wesnydy/testfiles/subsetsum/*."set.in";
    do
        echo "Teste $counter\n" >> $WLOG
        ./$WBIN 2 $i Wesnydy/testfiles/subsetsum/$counter.sum.in >> $WLOG
        echo >> $WLOG
        counter=$((counter+1))
    done

echo "# Executando o algoritmo Shortest...\n" | tee -a $WLOG

counter=1
for i in Wesnydy/testfiles/shortest/*."in";
    do
        echo "Teste $counter\n" >> $WLOG
        ./$WBIN 3 $i >> $WLOG
        echo >> $WLOG
        counter=$((counter+1))
    done

echo "# Executando o algoritmo PrimMst...\n" | tee -a $WLOG

counter=1
for i in Wesnydy/testfiles/primmst/*."in";
    do
        echo "Teste $counter\n" >> $WLOG
        ./$WBIN 4 $i >> $WLOG
        echo >> $WLOG
        counter=$((counter+1))
    done

echo "# Executando o algoritmo HuffmanCode...\n" | tee -a $WLOG

counter=1
for i in Wesnydy/testfiles/huffmacodes/*."in";
    do
        echo "Teste $counter\n" >> $WLOG
        ./$WBIN 5 $i >> $WLOG
        echo >> $WLOG
        counter=$((counter+1))
    done

echo "# Testes finalizados! Verifique a pasta 'log'\n"