#include <fstream>
#include <iostream>

#include "shortestpath.hpp"
#include "wordbreak.hpp"
#include "subsetsum.hpp"
#include "primmst.hpp"
#include "huffmancode.hpp"
// #include "abc.hpp"

using namespace std;

void print_matrix(int **matrix, int row, int column) {
  for (int i = 0; i < row; i++) {
    for (int j = 0; j < column; j++) {
      cout << matrix[i][j] << "  ";
    }
    cout << endl;
  }

  return;
}

// void bin_loader(const vector<int> &itens, vector<int> &bins, int capacity) {
//   int chosenbin;
//   int usedweigth;
//   int itensnum = itens.size();
//
//   int i;
//   int j;
//
//   for (i = 0; i < itensnum; i++) {
//     // clog << "Pegando o item: [" << itens.at(i) << "]\n";
//     chosenbin = rand() % itensnum;
//     for (j = chosenbin; j < itensnum; j = (j + 1) % itensnum) {
//       // clog << "Pacote selecionado aleatoriamente: [" << j << "]\n";
//       usedweigth = 100 - ((bins.at(j) * 100) / capacity);
//       // clog << "Pacote utilizado em: " << usedweigth << "%\n";
//       if (usedweigth < 50 && itens.at(i) <= bins.at(j)) {
//         // clog << "Item coube no pacote!\n\n";
//         bins.at(j) = bins.at(j) - itens.at(i);
//         break;
//       }
//       // clog << "Item não coube no pacote!\n";
//       // clog << "Tentando o próximo...\n\n";
//     }
//   }
//
//   for (i = 0; i < (signed) bins.size(); i++) {
//     if (bins.at(i) == capacity)
//       bins.erase(bins.begin() + i);
//   }
//
//   for (i = 0; i < (signed) bins.size(); i++) {
//     // clog << "Verificando Pacote [" << i << "]\n";
//     usedweigth = 100 - ((bins.at(i) * 100) / capacity);
//     // clog << "Pacote utilizado em: " << usedweigth << "%\n";
//     if (usedweigth <= 50) {
//       // clog << "Pacote pode ser mesclado" << "\n";
//       // clog << "Buscando outro pacote para mesclar" << "\n\n";
//       for (j = i + 1; j < (signed) bins.size(); j++) {
//         usedweigth = 100 - ((bins.at(j) * 100) / capacity);
//         // clog << "Pacote candidato [" << j << "]\n";
//         // clog << "Pacote utilizado em: " << usedweigth << "%\n";
//         if (usedweigth <= 50) {
//           // clog << "Pacote candidato pode ser mesclado" << "\n";
//           bins.at(i) = bins.at(i) - bins.at(j);
//           bins.erase(bins.begin() + j);
//           // clog << "Pacotes [" << i << "] e [" << j << "] mesclados\n\n";
//           break;
//         }
//         // clog << "Pacote não pode ser mesclado" << "\n\n";
//       }
//     }
//     // else { clog << "Pacote não pode ser mesclado" << "\n\n"; }
//   }
// }

int main(int argc, char **argv) {
  if (argc < 3) {
    cout << "Parâmetros insuficientes\n";
    return 1;
  }

  ifstream ifs;
  ifs.open(argv[2], ifstream::in);

  if (!ifs.is_open()) {
    cout << "Não foi possível abrir o arquivo, verifique se o mesmo existe\n";
    return 1;
  }

  switch (atoi(argv[1])) {
    case 1: { // Run Word Break Algorithm
      vector<string> dict_vec;
      string dict_word;

      cout << "Dicionário: [";
      while (getline(ifs, dict_word)) {
        cout << dict_word << ", ";
        dict_vec.push_back(dict_word);
      }
      cout << "]\n";

      ifs.close();

      WordBreak *wb = new WordBreak(dict_vec);

      ifs.open(argv[3], ifstream::in);
      if (!ifs.is_open()) {
        cout << "Só consegui ler o dicinário, verifique se o arquivo de entrada existe\n";
        return 1;
      }

      cout << "Resultado:\n";

      string word;
      while (getline(ifs, word)) {
        cout << word;
        wb->word_break(word)? cout <<" --> Yes\n": cout << " --> No\n";
      }
      break;
    }

    case 2: { // Run Subset Sum Algorithm
      int setsize;
      ifs >> setsize;

      vector<int> set;

      int value;
      cout << "Conjunto: [";
      for (int i = 0; i < setsize; i++) {
        ifs >> value;
        cout << value << ", ";
        set.push_back(value);
      }
      cout << "]\n";

      ifs.close();

      ifs.open(argv[3], ifstream::in);
      if (!ifs.is_open()) {
        cout << "Não foi possível abrir o arquivo com o valor da soma\n";
        return 1;
      }

      int sum;
      ifs >> sum;
      cout << "Valor da Soma: " << sum << "\n";

      int size = set.size();

      cout << "Resultado: ";
      if(is_subset_sum(set, size, sum))
        cout << "Found a subset with given sum\n";
      else
        cout << "No subset with given sum\n";
      break;
    }

    case 3: { // Run Shortest Path Algorithm
      int vertices;
      ifs >> vertices;

      int **matrix = new int*[vertices];
      for(int i = 0; i < vertices; i++)
        matrix[i] = new int[vertices];

      for (int i = 0; i < vertices; i++)
        for (int j = 0; j < vertices; j++)
          ifs >> matrix[i][j];

      cout << "Matriz de entrada:\n";
      print_matrix(matrix, vertices, vertices);

      cout << "\n";
      floyd_warshall(matrix, vertices);

      ifs.close();
      break;
    }

    case 4: { // Run Prim Mst Algorithm
      int vertices;
      ifs >> vertices;

      int **matrix = new int*[vertices];
      for(int i = 0; i < vertices; i++)
        matrix[i] = new int[vertices];

      for (int i = 0; i < vertices; i++)
        for (int j = 0; j < vertices; j++)
          ifs >> matrix[i][j];

      cout << "Matriz de entrada:\n";
      print_matrix(matrix, vertices, vertices);

      cout << "\nResultado:\n";
      prim_mst(matrix, vertices);

      ifs.close();
      break;
    }

    case 5: { // Run Ruffman Code Algorithm
      char item;
      int frequency;

      int size;
      ifs >> size;

      vector<char> itens;
      vector<int> itens_freq;

      cout << "Entrada: \n";
      for (int i = 0; i < size; i++) {
        ifs >> item;;
        cout << "Item: " << item;
        itens.push_back(item);
        ifs >> frequency;
        cout << " frequencia: " << frequency << "\n";
        itens_freq.push_back(frequency);
      }

      cout << "\nResultado: \n";
      huffman_codes(itens, itens_freq, size);

      ifs.close();
      break;
    }

    // case 6: { // Run Artificial Colony Bee algorithm
    //   vector<int> item;
    //   item.push_back(5);
    //   item.push_back(2);
    //   item.push_back(3);
    //   item.push_back(4);
    //   item.push_back(7);
    //   item.push_back(2);
    //   item.push_back(3);
    //
    //   vector<Bin*> bins;
    //
    //   for (unsigned int i = 0; i < item.size(); i++) {
    //     bins.push_back(new Bin(10));
    //   }
    //
    //   // int run;
    //   // double mean = 0;
    //   // srand(time(NULL));
    //
    //   Abc *abc = new Abc();
    //
    //   abc->bin_loader(item, bins, 10);

      // for (run = 0; run < RUNTIME; run++) {
      //   abc->initialize_food_sources();
      //   abc->memorize_best_source();
      //   for (int i = 0; i < MAXCICLE; i++) {
      //     abc->send_employed_bees();
      //     abc->calculate_probabilities();
      //     abc->send_onlook_bees();
      //     abc->memorize_best_source();
      //     abc->send_scout_bees();
      //   }
      //   for (int j = 0; j < PARAMSNUM; j++) {
      //     cout << "GlobalParam[" << j+1 << "]: " << abc->get_global_param(j) << endl;
      //   }
      //   double global_min = abc->get_global_min();
      //   cout << run + 1 << ". run: " << global_min << endl;
      //   abc->set_on_min_arr(run, global_min);
      //   mean = mean + global_min;
      // }
      // mean = mean / RUNTIME;
      // cout << "Means of " << RUNTIME << " runs: " << mean << endl;
    //   break;
    // }

    default:
      cout << "Seleção de algoritmo inválida\n";
      return 1;
  }

  return 0;
}
