#ifndef _ABC_HPP_
#define _ABC_HPP_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <iostream>
#include <vector>

#define COLONYSIZE 40
#define FOODNUMBER COLONYSIZE/2
#define LIMIT 100
#define MAXCICLE 3000

#define PARAMSNUM 50
#define LOWERBOUND -5.12
#define UPPERBOUND 5.12

#define RUNTIME 30

using namespace std;

double sphere(double sol[PARAMSNUM]);
double rosenbrock(double sol[PARAMSNUM]);
double griewank(double sol[PARAMSNUM]);
double rastrigin(double sol[PARAMSNUM]);

struct Bin {
  Bin() {}
  Bin(int capacity) : capacity_(capacity) {}
  ~Bin() {}

  int capacity_;
  vector<int> itens_;
};

class Abc {
 public:
    Abc() {}
    ~Abc() {}

    void initialize_food_sources();
    void memorize_best_source();
    void send_employed_bees();
    void calculate_probabilities();
    void send_onlook_bees();
    void send_scout_bees();

    double get_global_param(int index) const { return global_params_[index]; }
    double get_global_min() const { return global_min_; }

    void set_on_min_arr(int index, double value);

 private:
    void initialize_variables(int index);
    double calculate_fitness(double fun);

    /* BPP methods ans variables */
    void bin_loader(const vector<int> &itens, vector<Bin*> &bins, int capacity);

    vector<Bin*> bins;
    vector< vector<Bin*> > foods_;

    /* End of BPP methods ans variables */

    // double foods_[FOODNUMBER][PARAMSNUM];
    double f_[FOODNUMBER];
    double fitness_[FOODNUMBER];
    double trial_[FOODNUMBER];
    double prob_[FOODNUMBER];
    double solution_[PARAMSNUM];

    double obj_val_sol_;
    double fitness_sol_;

    int neighbour_;
    int param_to_change;

    double global_min_;
    double global_params_[PARAMSNUM];
    double global_mins_[RUNTIME];
    double random_;
};

#endif // _ABC_HPP_
