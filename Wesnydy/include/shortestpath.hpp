#ifndef _SHORTEST_PATH_HPP_
#define _SHORTEST_PATH_HPP_

#include <cstdio>

#define INFINITE 99999

using namespace std;

void floyd_warshall(int **graph, int v_num);
void print_solution(int **dist, int v_num);

#endif // _SHORTEST_PATH_HPP_
