#ifndef _SUBSET_SUM_H_
#define _SUBSET_SUM_H_

#include <vector>

using namespace std;

bool is_subset_sum(const vector<int> &set, int n, int sum);

#endif // _SUBSET_SUM_H_
