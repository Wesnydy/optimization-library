#ifndef _HUFFMAN_CODE_H_
#define _HUFFMAN_CODE_H_

#include <bits/stdc++.h>
#include <iostream>
#include <vector>

using namespace std;

// A Huffman tree node
struct MinHeapNode {
  char data;          // One of the input characters
  unsigned freq;      // Frequency of the character
  MinHeapNode *left;  // Left and right child
  MinHeapNode *right; // Left and right child

  MinHeapNode(char data, unsigned freq) {
    left = right = NULL;
    this->data = data;
    this->freq = freq;
  }
};
// For comparison of two heap nodes (needed in min heap)
struct compare {
  bool operator()(MinHeapNode *l, MinHeapNode *r) {
    return (l->freq > r->freq);
  }
};

void huffman_codes(const vector<char> &data, const vector<int> &freq, int size);
void print_codes(struct MinHeapNode *root, const string &str);

#endif // _HUFFMAN_CODE_H_
