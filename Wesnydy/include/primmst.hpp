#ifndef _PRIM_MINIMUM_SPANNING_TREE_HPP_
#define _PRIM_MINIMUM_SPANNING_TREE_HPP_

#include <climits>
#include <cstdio>

using namespace std;

void prim_mst(int **graph, int v_num);
int minimun_key(int *key, bool *mstset, int v_num);
void print_mst(int *parent, int **graph, int v_num);

#endif // _PRIM_MINIMUM_SPANNING_TREE_HPP_
