#ifndef _WORD_BREAK_HPP_
#define _WORD_BREAK_HPP_

#include <cstring>
#include <string>
#include <vector>

using namespace std;

class WordBreak {
 public:
   WordBreak(const vector<string> &dictionary);
   WordBreak();
   ~WordBreak();

   bool word_break(const string &word);

 private:
   bool dictionary_contains(const string &word);

   vector<string> dictionary_;
};

#endif // _WORD_BREAK_HPP_
