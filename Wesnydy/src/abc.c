#include "abc.hpp"

typedef double (*function_callback)(double sol[PARAMSNUM]);

function_callback function = &rastrigin;

double Abc::calculate_fitness(double fun) {
    double result = 0;
    if (fun >= 0) {
        result = 1 / (fun + 1);
    }
    else {
        result = 1 + fabs(fun);
    }
    return result;
}

void Abc::memorize_best_source() {
    for (int i = 0; i < FOODNUMBER; i++) {
        if (f_[i] < global_min_) {
            global_min_ = f_[i];
            for (int j = 0; j < PARAMSNUM; j++) {
                global_params_[j] = foods_[i][j];
            }
        }
    }
}

void Abc::initialize_variables(int index) {
    for (int j = 0; j < PARAMSNUM; j++) {
        random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
        foods_[index][j] = random_  * (UPPERBOUND - LOWERBOUND) + LOWERBOUND;
        solution_[j] = foods_[index][j];
    }
    f_[index] = function(solution_);
    fitness_[index] = calculate_fitness(f_[index]);
    trial_[index] = 0;
}

void Abc::initialize_food_sources() {
	int i;
    for(i = 0; i < FOODNUMBER; i++) {
	    initialize_variables(i);
	}
	global_min_ = f_[0];
    for(i = 0; i < PARAMSNUM; i++) {
        global_params_[i] = foods_[0][i];
    }
}

// void Abc::send_employed_bees() {
//     int i;
//     int j;
//
//     for ( i = 0; i < FOODNUMBER; i++) {
//         /*The parameter to be changed is determined randomly*/
//         random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
//         param_to_change = (int)(random_ * PARAMSNUM);
//
//         /*A randomly chosen solution is used in producing a mutant solution of the solution i*/
//         random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
//         neighbour_ =(int)(random_ * FOODNUMBER);
//
//         /*Randomly selected solution must be different from the solution i*/
//         while (neighbour_ == i) {
//             random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
//             neighbour_ = (int)(random_ * FOODNUMBER);
//         }
//
//         for(j = 0; j < PARAMSNUM; j++) {
//             solution_[j] = foods_[i][j];
//         }
//
//         /*v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij}) */
//         random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
//         solution_[param_to_change] = foods_[i][param_to_change]+(foods_[i][param_to_change]-foods_[neighbour_][param_to_change])*(random_-0.5)*2;
//
//         /*if generated parameter value is out of boundaries, it is shifted onto the boundaries*/
//         if (solution_[param_to_change] < LOWERBOUND)
//            solution_[param_to_change] = LOWERBOUND;
//         if (solution_[param_to_change] > UPPERBOUND)
//            solution_[param_to_change] = UPPERBOUND;
//         obj_val_sol_ = function(solution_);
//         fitness_sol_ = calculate_fitness(obj_val_sol_);
//
//         /*a greedy selection is applied between the current solution i and its mutant*/
//         if (fitness_sol_ > fitness_[i]) {
//             /*If the mutant solution is better than the current solution i, replace the solution with the mutant and reset the trial counter of solution i*/
//             trial_[i]=0;
//             for(j = 0; j < PARAMSNUM; j++)
//                 foods_[i][j] = solution_[j];
//             f_[i] = obj_val_sol_;
//             fitness_[i] = fitness_sol_;
//         }
//         else {   /*if the solution i can not be improved, increase its trial counter*/
//             trial_[i] = trial_[i] + 1;
//         }
//     }
// }

void Abc::send_employed_bees() {
  unsigned int i;
  unsigned int j;
  unsigned int item_index;
  unsigned int bin1_index;
  unsigned int bin2_index;
  int item_weigth;
  int bin_capacity;

  for (i = 0; i < FOODNUMBER; i++) { // lembrar de mudar foodnumber para a quantidade de itens parametizada
    // Selecionar um pacote aleatório no vetor de pacotes
    srand(time(NULL));
    bin1_index = rand() % foods_[i].size();
    // Seleciona um item aleatório no vetor de itens, do pacote selecionado antes
    item_index = rand() % foods_[i][bin1_index]->itens_.size();
    // Pega o peso do item
    item_weigth = foods_[i][bin1_index]->itens_.at(item_index);
    // Verifica se o item cabe em outro pacote selecionado aleatoriamente
    bin2_index = rand() % foods_[i].size();
    // Verifica se o pacote selecionado tem espaço para o item
    if (foods_[i][bin2_index]->capacity_ > item_weigth) {
      // Coloca o item no novo pacote
      foods_[i][bin2_index]->itens_.push_back(item_weigth);
      foods_[i][bin2_index]->capacity_ = foods_[i][bin2_index]->capacity_ - item_weigth;
      // Remove o item do pacote antigo
      foods_[i][bin1_index]->itens_.erase(foods_[i][bin1_index]->itens_.begin() + item_index);
      foods_[i][bin1_index]->capacity_ = foods_[i][bin1_index]->capacity_ + item_weigth;
      // Checagem para ver se o pacote antigo ficou com capacidade menor que 50%
      bin_capacity = 100 - ((foods_[i][bin1_index]->capacity_ * 100) / 10); // Lembrar de tirar o 10 e colocar o parametro de capacidade
      if (bin_capacity < 50) {
        // Aplica a rotina de mesclagem dos pacotes
        for (j = 0; j < foods_[i].size(); j++) {
          // Checagem para não comparar com o pacote selecionado
          if (j != bin1_index) {
            // Calcula a capacidade utilizada do pacote candidato
            bin_capacity = 100 - ((foods_[i][j]->capacity_ * 100) / 10);
            if (bin_capacity < 50) {
              // Mescla os pacotes
              foods_[i][j]->itens_.insert(foods_[i][j]->itens_.end(), foods_[i][bin1_index]->itens_.begin(), foods_[i][bin1_index]->itens_.end());
              foods_[i][j]->capacity_ = foods_[i][j]->capacity_ - foods_[i][bin1_index]->capacity_;
              // Remove o pacote vazio
              foods_[i].erase(foods_[i].begin() + bin1_index);
            }
          }
        }
      }
    }
  }
}

void Abc::calculate_probabilities() {
    int i;
    double maxfit;
    maxfit = fitness_[0];
    for (i = 1; i < FOODNUMBER; i++) {
        if (fitness_[i] > maxfit)
           maxfit = fitness_[i];
        }

    for (i = 0; i < FOODNUMBER; i++) {
        prob_[i] = (0.9 * (fitness_[i] / maxfit)) + 0.1;
    }
}

void Abc::send_onlook_bees() {
    int t = 0;
    int i = 0;
    int j;

    while(t < FOODNUMBER) {
        random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
        if(random_ < prob_[i]) { /*choose a food source depending on its probability to be chosen*/
            t++;
            /*The parameter to be changed is determined randomly*/
            random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
            param_to_change = (int) (random_ * PARAMSNUM);

            /*A randomly chosen solution is used in producing a mutant solution of the solution i*/
            random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)) );
            neighbour_ = (int)(random_ * FOODNUMBER);

            /*Randomly selected solution must be different from the solution i*/
            while(neighbour_ == i) {
                random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
                neighbour_ = (int)(random_ * FOODNUMBER);
            }

            for(j = 0; j < PARAMSNUM; j++) {
                solution_[j] = foods_[i][j];
            }

            /*v_{ij}=x_{ij}+\phi_{ij}*(x_{kj}-x_{ij}) */
            random_ = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
            solution_[param_to_change] = foods_[i][param_to_change]+(foods_[i][param_to_change]-foods_[neighbour_][param_to_change])*(random_-0.5)*2;

            /*if generated parameter value is out of boundaries, it is shifted onto the boundaries*/
            if (solution_[param_to_change] < LOWERBOUND)
                solution_[param_to_change] = LOWERBOUND;
            if (solution_[param_to_change] > UPPERBOUND)
                solution_[param_to_change] = UPPERBOUND;

            obj_val_sol_ = function(solution_);
            fitness_sol_ = calculate_fitness(obj_val_sol_);

            /*a greedy selection is applied between the current solution i and its mutant*/
            if (fitness_sol_ > fitness_[i]) {
                /*If the mutant solution is better than the current solution i, replace the solution with the mutant and reset the trial counter of solution i*/
                trial_[i] = 0;
                for(j = 0; j < PARAMSNUM; j++) {
                    foods_[i][j] = solution_[j];
                }
                f_[i] = obj_val_sol_;
                fitness_[i] = fitness_sol_;
            }
            else {   /*if the solution i can not be improved, increase its trial counter*/
                trial_[i] = trial_[i] + 1;
            }
        } /*if */

        i++;
        if (i == FOODNUMBER) {
            i = 0;
        }
    }
}

void Abc::send_scout_bees() {
    int maxtrialindex = 0;
    for (int i = 1; i < FOODNUMBER; i++) {
        if (trial_[i] > trial_[maxtrialindex]) {
            maxtrialindex = i;
        }
    }
    if(trial_[maxtrialindex] >= LIMIT) {
        initialize_variables(maxtrialindex);
    }
}

void Abc::bin_loader(const vector<int> &itens, vector<Bin*> &bins, int capacity) {
  int chosenbin;
  int usedweigth;
  int itensnum = itens.size();

  int i;
  int j;

  for (i = 0; i < itensnum; i++) {
    // clog << "Pegando o item: [" << itens.at(i) << "]\n";
    srand(time(NULL));
    while (true) {
      chosenbin = rand() % itensnum;
      // clog << "Pacote selecionado aleatoriamente: [" << chosenbin << "]\n";
      usedweigth = 100 - ((bins.at(chosenbin)->capacity_ * 100) / capacity);
      // clog << "Pacote utilizado em: " << usedweigth << "%\n";
      if (usedweigth < 50 && itens.at(i) <= bins.at(chosenbin)->capacity_) {
        // clog << "Item coube no pacote!\n\n";
        bins.at(chosenbin)->itens_.push_back(itens.at(i));
        bins.at(chosenbin)->capacity_ = bins.at(chosenbin)->capacity_ - itens.at(i);
        break;
      }
      // clog << "Item não coube no pacote!\n";
      // clog << "Tentando o próximo...\n\n";
    }
  }

  for (i = 0; i < (signed) bins.size(); i++) {
    if (bins.at(i)->capacity_ == capacity)
      bins.erase(bins.begin() + i);
  }

  for (i = 0; i < (signed) bins.size(); i++) {
    // clog << "Verificando Pacote [" << i << "]\n";
    usedweigth = 100 - ((bins.at(i)->capacity_ * 100) / capacity);
    // clog << "Pacote utilizado em: " << usedweigth << "%\n";
    if (usedweigth <= 50) {
      // clog << "Pacote pode ser mesclado" << "\n";
      // clog << "Buscando outro pacote para mesclar" << "\n\n";
      for (j = i + 1; j < (signed) bins.size(); j++) {
        usedweigth = 100 - ((bins.at(j)->capacity_ * 100) / capacity);
        // clog << "Pacote candidato [" << j << "]\n";
        // clog << "Pacote utilizado em: " << usedweigth << "%\n";
        if (usedweigth <= 50) {
          // clog << "Pacote candidato pode ser mesclado" << "\n";
          bins.at(i)->itens_.insert(bins.at(i)->itens_.end(), bins.at(j)->itens_.begin(), bins.at(j)->itens_.end());
          bins.at(i)->capacity_ = bins.at(i)->capacity_ - bins.at(j)->capacity_;
          bins.erase(bins.begin() + j);
          // clog << "Pacotes [" << i << "] e [" << j << "] mesclados\n\n";
          break;
        }
        // clog << "Pacote não pode ser mesclado" << "\n\n";
      }
    }
    // else { clog << "Pacote não pode ser mesclado" << "\n\n"; }
  }

  // for (i = 0; i < (signed) bins.size(); i++) {
  //   // clog << "Itens na caixa " << i << "\n";
  //   for (j = 0; j < (signed) bins.at(i)->itens_.size(); j++) {
  //     clog << bins.at(i)->itens_.at(j) << endl;
  //   }
  // }
}

void Abc::set_on_min_arr(int index, double value) {
    global_mins_[index] = value;
}

double sphere(double sol[PARAMSNUM]) {
    double top = 0;
    for (int j = 0; j < PARAMSNUM; j++) {
        top = top + sol[j] * sol[j];
    }
    return top;
}

double rosenbrock(double sol[PARAMSNUM]) {
    double top=0;
    for(int j = 0; j < PARAMSNUM-1; j++) {
        top = top+100*pow((sol[j+1]-pow((sol[j]),(double)2)),(double)2)+pow((sol[j]-1),(double)2);
    }
    return top;
}

 double griewank(double sol[PARAMSNUM]) {
	double top = 0;
    double top1 = 0;
    double top2 = 1;

    for(int j = 0; j < PARAMSNUM; j++) {
        top1 = top1 + pow((sol[j]),(double)2);
        top2 = top2 * cos((((sol[j])/sqrt((double)(j+1)))*M_PI)/180);
    }
	 top = (1/(double)4000) * top1 - top2 + 1;
	 return top;
 }

 double rastrigin(double sol[PARAMSNUM]) {
	 double top = 0;

	 for(int j = 0; j < PARAMSNUM; j++) {
		 top = top + (pow(sol[j],(double)2)-10*cos(2*M_PI*sol[j])+10);
	 }
	 return top;
 }
