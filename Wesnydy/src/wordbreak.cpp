#include "wordbreak.hpp"

WordBreak::WordBreak(const vector<string> &dictionary)
  : dictionary_(dictionary) {}

WordBreak::WordBreak() {}

WordBreak::~WordBreak() {}

bool WordBreak::dictionary_contains(const string &word) {
  int dict_size = dictionary_.size();
  for (int i = 0; i < dict_size; i++)
    if (dictionary_[i].compare(word) == 0)
      return true;

  return false;
}

bool WordBreak::word_break(const string &word) {
  int word_size = word.size();
  if (word_size == 0)
    return true;

  bool wb[word_size + 1];
  memset(wb, 0, sizeof(wb)); // Initialize all values as false.

  for (int i = 1; i <= word_size; i++) {
    // if wb[i] is false, then check if current prefix can make it true.
    // Current prefix is "str.substr(0, i)"
    if (wb[i] == false && dictionary_contains( word.substr(0, i) ))
      wb[i] = true;
      // wb[i] is true, then check for all substrings starting from
      // (i+1)th character and store their results.
    if (wb[i] == true) {
      if (i == word_size)
        return true;
      for (int j = i + 1; j <= word_size; j++) {
        // Note the parameter passed to dictionaryContains() is
        // substring starting from index 'i' and length 'j-i'
        if (wb[j] == false && dictionary_contains( word.substr(i, j-i) ))
          wb[j] = true;
        // If we reached the last character
        if (j == word_size && wb[j] == true)
          return true;
      }
    }
  }

  return false;
}
