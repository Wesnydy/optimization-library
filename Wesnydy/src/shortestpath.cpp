#include "shortestpath.hpp"

void floyd_warshall(int **graph, int v_num) {
  int i, j, k;

  int **dist = new int*[v_num];
  for(i = 0; i < v_num; i++) {
    dist[i] = new int[v_num];
  }

  for (i = 0; i < v_num; i++) {
    for(j = 0; j < v_num; j++) {
      dist[i][j] = graph[i][j];
    }
  }

  for(k = 0; k < v_num; k++)	{
    for(i = 0; i < v_num; i++)	{
      for(j = 0; j < v_num; j++)	{
        if(dist[i][k] + dist[k][j] < dist[i][j]) {
          dist[i][j] = dist[i][k] + dist[k][j];
        }
      }
    }
  }

  print_solution(dist, v_num);
}

void print_solution(int **dist, int v_num) {
  fprintf(stdout, "A matriz a seguir mostra a menor distância entre cada par de vértices:\n");
  for (int i = 0; i < v_num; i++) {
    for (int j = 0; j < v_num; j++) {
      if (dist[i][j] == INFINITE)
        fprintf(stdout, "%7s", "INF");
      else
        fprintf (stdout, "%7d", dist[i][j]);
    }
    fprintf(stdout, "\n");
  }
}
