#include "primmst.hpp"

int minimun_key(int *key, bool *mstset, int v_num) {
  int min_value = INT_MAX;
  int min_index = 0;

  for (int v = 0; v < v_num; v++)
    if (mstset[v] == false && key[v] < min_value) {
      min_value = key[v];
      min_index = v;
    }

  return min_index;
}

void print_mst(int *parent, int **graph, int v_num) {
  printf("Aresta   Peso\n");
  for (int i = 1; i < v_num; i++) {
    printf("%d---%d     %d \n", parent[i], i, graph[i][parent[i]]);
  }
}

void prim_mst(int **graph, int v_num) {
  int parent[v_num];  // Array to store constructed MST
  int key[v_num];     // Key values used to pick minimum weight edge in cut
  bool mstset[v_num]; // To represent set of vertices not yet included in MST

  // Initialize all keys as INFINITE
  for (int i = 0; i < v_num; i++) {
    key[i] = INT_MAX;
    mstset[i] = false;
  }

  // Always include first 1st vertex in MST.
  key[0] = 0; // Make key 0 so that this vertex is picked as first vertex
  parent[0] = -1; // First node is always root of MST

  for (int count = 0; count < v_num - 1; count++) {
    int u = minimun_key(key, mstset, v_num);
    mstset[u] = true;
    // Update key value and parent index of the adjacent vertices of
    // the picked vertex.
    for (int v = 0; v < v_num; v++) {
      // Update the key only if graph[u][v] is smaller than key[v]
      if (graph[u][v] && mstset[v] == false && graph[u][v] <  key[v]) {
        parent[v]  = u;
        key[v] = graph[u][v];
      }
    }
  }
  // print the constructed MST
  print_mst(parent, graph, v_num);
}
