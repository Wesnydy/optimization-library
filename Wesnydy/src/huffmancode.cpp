#include "huffmancode.hpp"

void print_codes(struct MinHeapNode *root, const string &str) {
  if (!root)
    return;

  if (root->data != '$')
    cout << root->data << ": " << str << endl;

  print_codes(root->left, str + "0");
  print_codes(root->right, str + "1");
}

void huffman_codes(const vector<char> &data, const vector<int> &freq, int size) {
  struct MinHeapNode *left;
  struct MinHeapNode *right;
  struct MinHeapNode *top;

  priority_queue<MinHeapNode*, vector<MinHeapNode*>, compare> min_heap;
  for (int i = 0; i < size; ++i) {
    min_heap.push(new MinHeapNode(data[i], freq[i]));
  }

  while (min_heap.size() != 1) {
    // Extract the two minimum freq items from min heap
    left = min_heap.top();
    min_heap.pop();
    right = min_heap.top();
    min_heap.pop();
    top = new MinHeapNode('$', left->freq + right->freq);
    top->left = left;
    top->right = right;
    min_heap.push(top);
  }

  print_codes(min_heap.top(), "");
}
