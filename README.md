# Biblioteca de otimização #

* Análise e Projeto de Algoritmos 2016.1
* Turma do prof. Leonardo Bezerra

### Compilando o projeto ###

 Para compilar o projeto, basta digitar o seguinte comando no terminal:
```sh
    $ make
```

## Executando os testes ###

 Para executar os testes, basta digitar o seguinte comando no terminal:
```sh
    $ ./run_tests.sh
```

>Nota: Se o script não executar, tente o seguinte comando ``` $ chmod +x run_tests.sh && ./run_tests.sh ```

### Contribuidores ###

* Claudiomar Araújo <claudiomarpda@gmail.com>
* Jonathan Rodrigues <jonathan.klstr@gmail.com>
* Wesnydy Ribeiro <wesnydy1@gmail.com>