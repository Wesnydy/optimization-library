// Date: 10/13/2016
// See README.txt for problem explanation.
// Time complexity: num_coins*value, which is O(mn)
// Space complexity: O(n)

#include "coin_change.hpp"

// @return the number of ways to reach 'value' with 'coins'
// @param coins[] are the coins values, e.g, {1, 2, 3}
// @param value is the changing value, i.e, the total to be reached combining coins
int count_max(int coins[], int num_coins, int value) {
    
    // the number of solutions for value i
    int table[value+1]; // one extra index for base case
 
    // Initialize all table values as 0
    memset(table, 0, sizeof(table));
    
    // Base case (if given value is 0)
    table[0] = 1;

    for(int i=0; i<num_coins; i++) { // for each coin
        for(int j=coins[i]; j<=value; j++) { // for j = base case to value
            table[j] += table[j-coins[i]]; // updates number of possible changes for the current coin according to given value
        }
    }
    return table[value];
}