//
// Created by mz on 23/11/16.
//

#include "Bin.hpp"
#include "Chromosome.hpp"
#include <fstream>

Bin::Bin(int capacity, int weight) : capacity(capacity) {
    totalWeight = weight;
}

int Bin::getCapacity() const {
    return capacity;
}

/**
 * Adds weight to the bin according to addWeight from Chromosome and first fit functions.
 * First fit function manages the BPP properties.
 * @param weightIndex
 * @param weight
 */
void Bin::addWeight(int weightIndex, int weight) {
    //printf("[BIN] add index %d and weight %d\n", weightIndex, weight);
    weightsIndex.push_back(weightIndex);
    totalWeight += weight;
}

int Bin::getTotalWeight() const {
    return totalWeight;
}

/**
 * Prints the weights stored in the bin (in weightIndex).
 * @param items is the original weight vector.
 */
void Bin::printWeightIndexes(vector<int> items, ofstream &outFile, bool writeToFile) {
    int i = 0;
    for (auto &wi  : weightsIndex) {
        if (totalWeight > capacity) {
            if (writeToFile) {
                outFile << "ERROR: totalWeight " << totalWeight << " >  capacity " << capacity << endl;
            } else {
                cout << "ERROR: totalWeight " << totalWeight << " >  capacity " << capacity << endl;
            }
        }
        if (writeToFile) {
            outFile << "weightIndex[" << i << "]: "<<wi<<"; items: "<<items[wi]<< endl;
        } else {
            printf("weightIndex[%d]: %d; items: %d \n", i, wi, items[wi]);
        }
        i++;
    }
}

/**
 * Compares the current bin to the bin from argument b, according to the indexes that they store.
 * @param b  the bin to be compared with.
 * @return true if they have at least one equal index, false otherwise.
 */
bool Bin::compare(Bin b) {
    for (auto &v1  : weightsIndex) {
        for(auto &v2  : b.weightsIndex){
            if(v1 == v2){
                return true;
            }
        }
    }
    return false;
}

/**
 * Sets the indexes of the weight as used.
 * @param crossWeightIndex is the map to the indexes of the original weight vector.
 */
void Bin::setCrossWeightIndex(int *crossWeightIndex) {
    for(int i=0; i<weightsIndex.size(); i++){
        crossWeightIndex[weightsIndex[i]] = WEIGHT_STORED;
    }
}

/**
 * Checks if the indexes of the weight vector stored in the bin is already used.
 *
 * @param crossWeightIndex is the map to the indexes of the original weight vector.
 * @return true if at least one index is used, false otherwise.
 */
bool Bin::wasCrossWeightIndexUsed(int *crossWeightIndex) {
    for (auto &i  : weightsIndex) {
        //printf("crosswWeightIndex[%d] = %d\n", i, crossWeightIndex[i]);
        if(crossWeightIndex[i] == WEIGHT_STORED){
            return true;
        }
    }
    return false;
}