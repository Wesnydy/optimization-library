// Date: 10/14/2016
// See README.txt for more details
// Adjacency matrix representation of the graph.
// Time complexity: O(VERTICES^2)
// Space complexity: O(VERTICES^2)

#include "dijkstra_shortest_path.hpp"  

// Finds the vertex with minimum distance value from
// the set of vertices not yet included in shortest path tree
int min_distance(int distance[], bool spt_set[]) {

   int min = INT_MAX;
   int min_index;
  
   for (int v = 0; v < VERTICES; v++) {
      if (spt_set[v] == false && distance[v] <= min) {
         min = distance[v], min_index = v;
      }
   }
 
   return min_index;
}
  
// Constructed distance array
void print_solution(int distance[], int num_vertices) {

    printf("Vertex   Distance from Source\n");
    for (int i = 0; i < num_vertices; i++) {
        printf("%d \t\t %d\n", i, distance[i]);
    }
}
  
// Adjacency matrix representation
void dijkstra(int graph[VERTICES][VERTICES], int src) {

    // The output array that will hold the shortest distance from src to i
    int distance[VERTICES]; 

    bool spt_set[VERTICES]; 
    // spt_set[i] is true if vertex i is included in shortest
    // path tree or shortest distance from src to i is finalized
  
    // Initialize all distances as INFINITE and stpSet[] as false
    for (int i = 0; i < VERTICES; i++){
        distance[i] = INT_MAX, spt_set[i] = false;
    }

    // Distance of source vertex from itself is always 0
    distance[src] = 0;

    // Find shortest path for all vertices
    for (int count = 0; count < VERTICES-1; count++) {
        // Pick the minimum distance vertex from the set of vertices not
        // yet processed. u is always equal to src in first iteration.
        int u = min_distance(distance, spt_set);

        // Mark the picked vertex as processed
        spt_set[u] = true;

        // Update distance value of the adjacent vertices of the picked vertex.
        for (int v = 0; v < VERTICES; v++){

            // Update distance[v] only if is not in spt_set, there is an edge from 
            // u to v, and total weight of path from src to  v through u is 
            // smaller than current value of distance[v]
            if (!spt_set[v] && graph[u][v] && distance[u] != INT_MAX 
                                           && distance[u]+graph[u][v] < distance[v]){
                distance[v] = distance[u] + graph[u][v];
            }
        }
    }

    // print the constructed distance array
    print_solution(distance, VERTICES);
}