// Date: 10/14/2016
// See README.txt for problem explanation.
// Space complexity is 3n, i.e, O(n)
// Time complexity: O(nlogn) to sort boxes. O(n^2) to apply DP. Hence it is O(n^2).

#include "box_stacking.hpp"

// We use qsort() to sort boxes in decreasing order of base area.
// For more details, see
// http://www.cplusplus.com/reference/cstdlib/qsort/ 
int compare(const void* a, const void* b){
	return ( (*(Box *)b).l * (*(Box *)b).w ) -
           ( (*(Box *)a).l * (*(Box *)a).w );}

// Returns the maximum value of a and b
int max (int a, int b) { 
    if(a > b)
      return a;
    else
      return b; 
}

// Returns the minimum value of a and b
int min (int a, int b) {
    if(a < b)
      return a;
    else
      return b; 
}

// Returns the greater height gotten from the combination of given boxes
int maxStackHeight(Box v[], int n) {

    // Create an array of all rotations of given boxes
    Box rotation[3*n];
    int index = 0;
    for (int k = 0; k < n; k++) {
        // Copy the original box
        rotation[index] = v[k];
        index++;

        // First rotation of box
        rotation[index].h = v[k].w;
        rotation[index].l = max(v[k].h, v[k].l);
        rotation[index].w = min(v[k].h, v[k].l);
        index++;

        // Second rotation of box
        rotation[index].h = v[k].l;
        rotation[index].l = max(v[k].h, v[k].w);
        rotation[index].w = min(v[k].h, v[k].w);
        index++;
   }
 
    // Now the number of boxes is 3n
    n = 3*n;

    /* Sort the array ‘rotation[]’ in decreasing order using library function*/
    qsort (rotation, n, sizeof(rotation[0]), compare);

    printf("\nRotations");
    printf("\nh x w x l\n\n");
    for (int k = 0; k < n; k++ )
      printf("%d x %d x %d\n", rotation[k].h, rotation[k].w, rotation[k].l);

    /* Initialize max_height values for all indexes 
      max_height[k] –> Maximum possible Stack Height with box k on top */
    int max_height[n];
    for (int k = 0; k < n; k++ )
      max_height[k] = rotation[k].h;

    /* Compute optimized max_height values in bottom up manner */
    for (int k = 1; k < n; k++ ){
      for (int j = 0; j < k; j++ ) {
          if ( rotation[k].w < rotation[j].w &&
              rotation[k].l < rotation[j].l &&
              max_height[k] < max_height[j] + rotation[k].h
            ) {
              max_height[k] = max_height[j] + rotation[k].h;
          }
      }
    } 
 
    /* Pick maximum of all max_height values */
    int max = -1;
    for (int k = 0; k < n; k++){
      if (max < max_height[k]){
         max = max_height[k];
      }
    }

    return max;
}
