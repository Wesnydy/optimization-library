//
// Created by mz on 27/11/16.
//

#include "BPP_GA.hpp"

using namespace std;

/**
 * Reads the instance.
 * @param filePath
 * @return a vector with weight values.
 */
vector<int> readFile(string filePath) {
    ifstream inFile(filePath, ios::in);

    int value;
    inFile >> value;
    vector<int> in(value + 2); // +2 for (1. the number of items; 2. the max bin capacity)
    in[0] = value;
    inFile >> value;
    in[1] = value;

    for (int i = 2; inFile >> value; i++) {
        in[i] = value;
    }
    return in;
}


/**
 * Checks if the vector contains de value
 * @param v
 * @param value
 * @return true if it contains, false otherwise.
 */
bool containsValue(vector<int> v, int value) {
    for (int i = 2; i < v.size(); i++) { // indexes 0 and 1 are not items' weight
        if (v[i] == value) {
            return true;
        }
    }
    return false;
}

/**
 * Random walk.
 * Randomizes the items' index and stores in a vector,
 * to change the order which the items will be inserted in the bins.
 * @param items weight. The instance.
 * @return the vector with the indexes of items in aleatory order.
 */
vector<int> randomizeItemsSequence(vector<int> items) {
    vector<int> randomVector(items.size());
    int maxPossibleIndex = items[0]; // v[0] is the possible maximum index of the main old_instances vector
    int randomIndex;


    /*
     * Index range: 0 to 49. The size of the vector is 52. So, 52-3 = 49
     * Why -3: -2 is for the first two positions that are not items value.
     */
    randomVector[0] = items[0];
    randomVector[1] = items[1];
    for (int i = 2; i < items.size();) {

        // +2 because we don't need index 0 and 1.
        randomIndex = (rand() % maxPossibleIndex) + 2;
        if (!containsValue(randomVector, randomIndex)) {
            randomVector[i] = randomIndex;
            i++;
        }
    }
    return randomVector;
}

/**
 * Finds the lower bound of the items' weight (the instance).
 * @param items with weight. They are the instance.
 * @param binCapacity
 * @return the lower bound.
 */
int getLowerBound(vector<int> items) {
    int c = items[1]; // Bin's capacity
    int totalWeight = 0;

    for (int i = 2; i < items.size(); i++) {
        totalWeight += items[i];
    }
    if (totalWeight % c > 0) { // there is rest of division
        return (totalWeight / c) + 1; // rounded to more
    } else {
        // no rest of division
        return totalWeight / c;
    }
}


/**
 * Inserts items in the bins according to First Fit algorithm. May not be the best solution.
 * @See http://www.geeksforgeeks.org/bin-packing-problem-minimize-number-of-used-bins/
 *
 * @param weight is the vector with all items' weight.
 * @return the minimum number of bins necessary to store all items.
 */
int firstFit(vector<int> weight, vector<int> iRandom, Chromosome &chromosome) {
    int c = weight[1]; // bin's capacity


    // Initialize result (Count of bins)
    int res = 0;

    // Create an array to store remaining space in bins
    // there can be at most n bins
    int bin_rem[iRandom.size() - 2];

    // Place items one by one
    for (int i = 2; i < iRandom.size(); i++) {
        // Find the first bin that can accommodate
        // weight[i]
        int j;
        for (j = 0; j < res; j++) {
            if (bin_rem[j] >= weight[iRandom[i]]) {
                bin_rem[j] = bin_rem[j] - weight[iRandom[i]];
                chromosome.addWeight(j, iRandom[i], weight[iRandom[i]]);
                break;
            }
        }

        // If no bin could accommodate weight[i]
        if (j == res) {
            chromosome.addWeight(res, iRandom[i], weight[iRandom[i]]);
            bin_rem[res] = c - weight[iRandom[i]];
            res++;
        }
    }
    return res;
}


/**
 * Creates two chromosomes and does the crossover operation.
 * TODO: Populate more chromosomes and find the best of them.
 * Why: If looking for optimal as a more frequent result.
 * However, the current algorithm already gives solutions near to optimal,
 * and sometimes the optimal one.
 *
 * @param weight the instance
 * @param lowerBound
 * @return the new Chromosome from crossover operation.
 */
Chromosome populate(vector<int> weight) {

    Chromosome ch1(weight[1]);
    vector<int> randomIndexes1 = randomizeItemsSequence(weight);
    firstFit(weight, randomIndexes1, ch1);

    vector<int> randomIndexes2 = randomizeItemsSequence(weight);
    Chromosome ch2(weight[1]);
    firstFit(weight, randomIndexes2, ch2);

//    cout << "populating..." << endl;
//    cout << "ch1 total weight: " << ch1.getTotalWeight();
//    cout << "\t number of bins: " << ch1.getNumberOfBins() << endl;
//
//    cout << "ch2 total weight: " << ch2.getTotalWeight();
//    cout << "\t number of bins: " << ch2.getNumberOfBins() << endl << endl;

    return ch1.crossover(ch2, weight);
}
