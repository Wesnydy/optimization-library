//
// Created by mz on 22/11/16.
//

#include "Chromosome.hpp"
#include <fstream>

/**
 * Chromosome constructor.
 * @param capacity is the bin capacity.
 */
Chromosome::Chromosome(int capacity) : binCapacity(capacity) {
    totalWeight = 0;
}

int Chromosome::getTotalWeight() const {
    return totalWeight;
}

int Chromosome::getNumberOfBins() {
    return (int) bins.size();
}

/**
 * Adds weights to the chromosome according to the index gotten.
 * The first fit manages the BPP properties.
 * @param binIndex
 * @param weightIndex is the index of the weight main vector.
 * @param weight is the real weight to sum to total weight.
 */
void Chromosome::addWeight(int binIndex, int weightIndex, int weight) {
    //printf("[CHROMOSOME] add bin %d, index %d weight %d \n", binIndex, weightIndex, weight);
    if (binIndex >= bins.size()) {
        //cout << "new bin" << endl;
        Bin b(binCapacity);
        b.addWeight(weightIndex, weight);
        bins.push_back(b);
        //cout << bins.size() << " sizeee" << endl;
    } else {
        //cout << "existent bin" << endl;
        bins.at((unsigned long) binIndex).addWeight(weightIndex, weight);
    }
    totalWeight += weight;
}

/**
 * Prints all bins and their weight.
 * @param weight is the original weight vector.
 */
void Chromosome::printBins(vector<int> weight, ofstream &outFile, bool writeToFile) {
    int i = 0;
    for (auto &item  : bins) {
        if (writeToFile) {
            outFile << "bin[" << i << "] \t Total weight: " << item.getTotalWeight() << endl;
        } else {
            printf("bin[%d] \t Total weight: %d\n", i, item.getTotalWeight());
        }
        item.printWeightIndexes(weight, outFile, writeToFile);
        if(writeToFile){
            outFile << endl;
        }
        else{
            cout << endl;
        }
        i++;
    }
}

/**
 * Gets the best bins of each chromosome and combines in a new one.
 * If the bin does't reach the acceptance rate, it is not used.
 * In the end of the crossover, the missing bins are inserted
 * in the chromosome with firstFitCrossover.
 *
 * Besides the number of bins to check the integrity of the solution
 * (number of bins must be >= lower bound),
 * the total weight of the chromosome is another point to be analysed.
 *
 * @param chr is the second chromosome to be crossed with this instance.
 * @param items is the original weight vector.
 * @return the new Chromosome.
 */
Chromosome Chromosome::crossover(Chromosome &chr, vector<int> items) {
    Chromosome newChr(binCapacity);
    double b1AdaptationLevel = 0;
    double b2AdaptationLevel = 0;

    // +2 for the first 2 values of the main weight vector
    // maps the indexes that are already used
    int crossWeightIndex[items[1] + 2];

    for (auto &b1 : bins) {
        b1AdaptationLevel = (double) b1.getTotalWeight() / binCapacity;

        for (auto &b2  : chr.bins) {
            b2AdaptationLevel = (double) b2.getTotalWeight() / binCapacity;

            // neither bin1 or bin2 is acceptable
            if (b1AdaptationLevel <= ACCEPTANCE_RATE && b2AdaptationLevel <= ACCEPTANCE_RATE) {
                continue;
            } else if (b1AdaptationLevel <= ACCEPTANCE_RATE) { // bin1 failed
                // tries to add bin2
                if (b2AdaptationLevel >= ACCEPTANCE_RATE) { // bin2 is ok
                    tryToAddBinCrossover(b2, crossWeightIndex, newChr);
                }
            } else if (b2AdaptationLevel <= ACCEPTANCE_RATE) { // bin2 failed
                // tries to add bin1
                if (b1AdaptationLevel >= ACCEPTANCE_RATE) { // bin1 is ok
                    tryToAddBinCrossover(b1, crossWeightIndex, newChr);
                }
            } else { // both bin1 and bin 2 are acceptable

                // can add both
                if (!b1.compare(b2)) { // they don't have at least one equal index

                    // OPTION 1
                    // tries to adds the best bin
                    /*if (b1AdaptationLevel >= b2AdaptationLevel) {
                        tryToAddBinCrossover(b1, crossWeightIndex, newChr);
                    } else {
                        tryToAddBinCrossover(b2, crossWeightIndex, newChr);
                    }*/

                    // OPTION 2
                    // tries to add both bins
                    tryToAddBinCrossover(b2, crossWeightIndex, newChr);
                    tryToAddBinCrossover(b1, crossWeightIndex, newChr);

                }// else case was already handled above
            }
        }
    }

//    cout << "before first fit crossover";
//    cout << "\ttotal weight: " << newChr.getTotalWeight();
//    cout << "\tnumber of bins: " << newChr.getNumberOfBins() << endl;

    // Cross over finished. Now we need to add the missing values.
    firstFitCrossover(newChr, items, crossWeightIndex);

//    cout << "after first fit crossover";
//    cout << "\ttotal weight: " << newChr.getTotalWeight();
//    cout << "\tnumber of bins: " << newChr.getNumberOfBins() << endl << endl;

    return newChr;
}

/**
 * Inserts instances of Bins with weights already stored
 * @param bin
 * @param newChr gets
 */
void Chromosome::addBinCrossOver(Bin bin, Chromosome &newChr) {
    bins.push_back(bin);
    newChr.totalWeight += bin.getTotalWeight();
}

/**
 * Try to add bin in new chromosome. Adds only if no weight of the bin
 * is already marked as used at crossWeightIndex.
 * @param bin
 * @param crossWeightIndex maps the indexes that are already used
 * @param newChr is the Chromosome that will store the bin
 */
void Chromosome::tryToAddBinCrossover(Bin bin, int *crossWeightIndex, Chromosome &newChr) {
    if (!bin.wasCrossWeightIndexUsed(crossWeightIndex)) { // no weight index of the bin was used
        newChr.addBinCrossOver(bin, newChr); // adds bin to the new chromosome
        bin.setCrossWeightIndex(crossWeightIndex); // sets the indexes of the bin as used
    }
}

/**
 * Finishes the insertions in the chromosome using all available items's weight.
 *
 * @param chromosome is almost finished from crossOver
 * @param weight are all weights
 * @param crossWeightIndex maps the indexes that are already used.
 * @return the chromosome with all items.
 */
int Chromosome::firstFitCrossover(Chromosome &chromosome, vector<int> weight, int *crossWeightIndex) {
    int c = weight[1]; // bin's capacity

    int numberOfBins = chromosome.getNumberOfBins();
    // Initialize result (Count of bins)
    int res = numberOfBins;

    // Create an array to store remaining space in bins
    // there can be at most n bins

    int bin_rem[weight.size() - 2];


    // Place items one by one
    for (int i = 2; i < weight.size(); i++) {
        if (crossWeightIndex[i] == WEIGHT_STORED) {
            continue;
        }
        // Find the first bin that can accommodate
        // weight[i]
        int j;
        for (j = numberOfBins; j < res; j++) {
            if (bin_rem[j] >= weight[i]) {
                bin_rem[j] = bin_rem[j] - weight[i];
                chromosome.addWeight(j, i, weight[i]);
                //printf("%d %d \n", i, weight[i]);
                break;
            }
        }

        // If no bin could accommodate weight[i]
        if (j == res) {
            chromosome.addWeight(res, i, weight[i]);
            bin_rem[res] = c - weight[i];
            res++;
        }
    }
    return res;
}