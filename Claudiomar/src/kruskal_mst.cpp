// Date: 10/15/2016
// Kruskal's Minimum Spanning Tree
// Time complexity: O(E.log(E))
// Space complexity: O(E+V) disjoint sets + result storage

#include "kruskal_mst.hpp"

// Creates a graph with vertices and edges
Graph* createGraph(int vertices, int edges) {
    Graph* graph = ( Graph*) malloc( sizeof( Graph) );
    graph->vertices = vertices;
    graph->edges = edges;
 
    graph->edge = ( Edge*) malloc( graph->edges * sizeof( Edge ) );
 
    return graph;
}
 
// Find set of an element i
// Uses path compression technique
int find_set(Subset subsets[], int i) {
    // Find root and make root as parent of i (path compression)
    if (subsets[i].parent != i) {
        subsets[i].parent = find_set(subsets, subsets[i].parent);
    }
 
    return subsets[i].parent;
}
 
// Union of two sets by rank
void union_sets(Subset subsets[], int x, int y) {
    int xroot = find_set(subsets, x);
    int yroot = find_set(subsets, y);
 
    // Attach smaller rank tree under root of high rank tree
    // (Union by rank)
    if (subsets[xroot].rank < subsets[yroot].rank){
        subsets[xroot].parent = yroot;
    }
    else if (subsets[xroot].rank > subsets[yroot].rank){
        subsets[yroot].parent = xroot;
    }
 
    // If ranks are same, then make one as root and increment
    // its rank by one
    else {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}
 
// Compare two edges according to their weights.
// Used in qsort() for sorting an array of edges.
// http://www.cplusplus.com/reference/cstdlib/qsort/
int kcompare(const void* a, const void* b) {
    Edge* a1 = (struct Edge*)a;
    Edge* b1 = (struct Edge*)b;
    return a1->weight > b1->weight;
}
 
// Construct MST using Kruskal's algorithm
void kruskal_mst(Graph* graph) {
    int vertices = graph->vertices;
    struct Edge result[vertices];  // Resultant MST
    int e = 0;  // An index variable, used for result[]
    int i = 0;  // An index variable, used for sorted edges
 
    // Step 1:  Sort all the edges in non-decreasing order of their weight
    // If we are not allowed to change the given graph, we can create a copy of
    // array of edges
    qsort(graph->edge, graph->edges, sizeof(graph->edge[0]), kcompare);
 
    // Allocate memory for creating vertices ssubsets
    Subset *subsets = (Subset*) malloc( vertices * sizeof(Subset) );
 
    // Create vertices subsets with single elements
    for (int v = 0; v < vertices; ++v) {
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }
 
    // Number of edges to be taken is equal to vertices-1
    while (e < vertices - 1) {
        // Step 2: Pick the smallest edge. And increment the index
        // for next iteration
        Edge next_edge = graph->edge[i++];
 
        int x = find_set(subsets, next_edge.source);
        int y = find_set(subsets, next_edge.destination);
 
        // If including this edge does't cause cycle, include it
        // in result and increment the index of result for next edge
        if (x != y) {
            result[e++] = next_edge;
            union_sets(subsets, x, y);
        }
        // Else, discard the next_edge
    }
 
    // Prints the contents of result[]
    printf("Edges in the constructed MST\n");
    for (i = 0; i < e; ++i)
        printf("%d -- %d == %d\n", result[i].source, result[i].destination,
                                                   result[i].weight);
    return;
}