#include <iostream>
#include <stdlib.h>
#include "pascal_triangle.hpp"
#include "coin_change.hpp"
#include "box_stacking.hpp"
#include "dijkstra_shortest_path.hpp"
#include "kruskal_mst.hpp"
#include "BPP_GA.hpp"
#include "Chromosome.hpp"
#include <ctime>

using namespace std;

void _pascal_triangle(){
	pascal_triangle(1);
	cout << "-------" << endl;
	pascal_triangle(2);
	cout << "-------" << endl;
	pascal_triangle(3);
	cout << "-------" << endl;
	pascal_triangle(5);
	cout << "-------" << endl;
	pascal_triangle(10);
	cout << "-------" << endl;
	pascal_triangle(15);
	cout << "-------" << endl;
	pascal_triangle(20);
}

void _coin_change(){
	// Coin of 0 value must not be considered, i.e, int coins[] = {0};

    // Let's start with one coin of value 1 
    // Number of possibilities: 1; (0)
    int coins[] = {1};
    int number_of_coins = 1;
    int value = 0;
    printf("coins {%d}, value: %d, max possibilities %d\n", 
        coins[0], value, count_max(coins, number_of_coins, value));

    // Now, value 1 for the same coin
    // Number of possibilities: 1; (1)
    value = 1;
    printf("coins {%d}, value: %d, max possibilities %d\n", 
        coins[0], value, count_max(coins, number_of_coins, value));

    // Now, value 2 for the same coin
    // Number of possibilities: 1; (1,1)
    value = 2;
    printf("coins {%d}, value: %d, max possibilities %d\n\n", 
        coins[0], value, count_max(coins, number_of_coins, value));
    // There will be only one way for 3, 4...n values


    // -------------------------------------------------------------------


    // Now, lets try 3 coins of values 1, 2 and 3, respectively.
    // Number of possibilities: 1; (1)
    int coins_2[] = {1, 2, 3};
    int number_of_coins_2 = 3;
    int value_2 = 1;
    printf("coins {%d, %d, %d}, value: %d, max possibilities %d\n", 
        coins_2[0], coins_2[1], coins_2[2], value_2, count_max(coins_2, number_of_coins_2, value_2));

    // Now, value 2 for the same coins
    // Number of possibilities: 2; (1, 1), (2)
    value_2 = 2;
    printf("coins {%d, %d, %d}, value: %d, max possibilities %d\n", 
        coins_2[0], coins_2[1], coins_2[2], value_2, count_max(coins_2, number_of_coins_2, value_2));

    // Now, value 3 for the same coins
    // Number of possibilities: 3; (1, 1, 1), (1, 2), (3)
    value_2 = 3;
    printf("coins {%d, %d, %d}, value: %d, max possibilities %d\n", 
        coins_2[0], coins_2[1], coins_2[2], value_2, count_max(coins_2, number_of_coins_2, value_2));

    // Now, value 4 for the same coins
    // Number of possibilities: 4; (1, 1, 1, 1), (1, 1, 2), (2, 2), (1, 3)
    value_2 = 4;
    printf("coins {%d, %d, %d}, value: %d, max possibilities %d\n", 
        coins_2[0], coins_2[1], coins_2[2], value_2, count_max(coins_2, number_of_coins_2, value_2));

    // Now, value 5 for the same coins
    // Number of possibilities: 5; (1, 1, 1, 1, 1), (1, 1, 1, 2), (1, 2, 2), (1, 1, 3), (2, 3)
    value_2 = 5;
    printf("coins {%d, %d, %d}, value: %d, max possibilities %d\n", 
        coins_2[0], coins_2[1], coins_2[2], value_2, count_max(coins_2, number_of_coins_2, value_2));	
}

void _box_stacking(){
	// Here we use the same example of README.txt to show that 
    //they have the same possible boxes, giving the expected result
    Box v[] = { {1, 2, 4}, {3, 2, 5} };
    int n = sizeof(v)/sizeof(v[0]);
    printf("The maximum height is %d\n", maxStackHeight(v, n) );

    // What about one box with height 9? The answer is 9.
    // Because there is only one box to avaliate, and the answer is straightforward.
    Box v2[] = {9, 6, 3};
    printf("The maximum height is %d\n", maxStackHeight(v2, 1) );
}

void _dijkstra_shortest_path(){
    // Now we can run the example from README.txt
    // The value of VERTICES must be 6 for this example
    //   a  b  c  d  e  f

    int graph[VERTICES][VERTICES] = {
        {0, 5, 0, 9, 2, 0},
        {5, 0, 2, 0, 0, 0},
        {0, 2, 0, 3, 0, 0},
        {9, 0, 3, 0, 0, 2},
        {2, 0, 0, 0, 0, 3},
        {0, 0, 0, 2, 3, 0}
    };

    dijkstra(graph, 0);
}

void _kruskal_mst(){
	// Let's build the undirected, connected and weighted graph 
    // from README.txt example.
    // As you will see, the result will be the same as the theory
    // shown there.

    int vertices = 6;  // Number of vertices in graph
    int edges = 9;  // Number of edges in graph
    struct Graph* graph = createGraph(vertices, edges);
/*    

Edges connecting vertices and their weights
------
AD - 1 
BC - 1
CD - 1
EF - 2
BD - 3
AB - 3 
CF - 4
CE - 5
DE - 6
------

Characters will be represented as the following numbers in the result:
A = 0
B = 1
C = 2
D = 3
E = 4
F = 5

*/ 
    // add edge
    graph->edge[0].source = 0; // A
    graph->edge[0].destination = 3; // AD
    graph->edge[0].weight = 1;
 
    // add edge
    graph->edge[1].source = 0; // A
    graph->edge[1].destination = 1; // AB
    graph->edge[1].weight = 3;

    // add edge
    graph->edge[2].source = 1; // B
    graph->edge[2].destination = 2; // BC
    graph->edge[2].weight = 1;
 
       // add edge
    graph->edge[3].source = 1; // B
    graph->edge[3].destination = 3; // BD
    graph->edge[3].weight = 3;

    // add edge
    graph->edge[4].source = 2; // C
    graph->edge[4].destination = 3; // CD
    graph->edge[4].weight = 1;

    // add edge
    graph->edge[5].source = 2; // C
    graph->edge[5].destination = 4; // CE
    graph->edge[5].weight = 5;

        // add edge
    graph->edge[6].source = 2; // C
    graph->edge[6].destination = 4; // CF
    graph->edge[6].weight = 4;
    
    // add edge
    graph->edge[7].source = 3; // D
    graph->edge[7].destination = 4; // DE
    graph->edge[7].weight = 6;

    // add edge
    graph->edge[8].source = 4; // E
    graph->edge[8].destination = 5; // EF
    graph->edge[8].weight = 2;
 
    kruskal_mst(graph);

}

/**
* Reads the instance from a file and creates the main vector of items.
* Calls populate function to create Chromosomes and does the crossover operation with them.    
*/
void _bpp_ga(int fileNumber){

    clock_t begin, end;
    begin = clock();
    double elapsedSeconds;

     // Initialize random seed
    srand(time(NULL)); // Must be initialized only once to generate different values.
    ofstream outFile;

    const string IN_PATH = "src/bpp-ga/instances/bpp" + to_string(fileNumber) + ".BPP";
    const string OUT_PATH = "src/bpp-ga/log/bpp"+to_string(fileNumber)+".log";

    cout << "Input file: " << IN_PATH << endl;
    outFile.open(OUT_PATH);
    outFile <<"Input: " << IN_PATH << endl;
    outFile << "-----------------------" << endl << endl;

    vector<int> weight = readFile(IN_PATH);
    int lowerBound = getLowerBound(weight);

    Chromosome chr = populate(weight);

    end = clock();
    elapsedSeconds = (double(end - begin) / CLOCKS_PER_SEC);

    chr.printBins(weight, outFile, true);
    
    outFile << "Capacity: " << weight[1] << endl;
    outFile << "Lower bound: " << lowerBound << endl << endl;

    outFile << "Total weight: " << chr.getTotalWeight() << endl;
    outFile << "Number of bins: " << chr.getNumberOfBins() << endl;
    outFile << "Time: " << elapsedSeconds << " seconds" << endl;
    outFile << "---------------------" << endl;
  
    
    outFile.close();
    cout << "Output writen to file " << OUT_PATH << endl;
    
    
}

int main(int argc, char *argv[]){
    cout << argc << " argc"<< endl; 
    if (argc < 3) {
		printf("Parâmetros Insuficientes");
        cout << "1: numero_de_parametros" << endl;
        cout << "2: numero_do_algoritmo" << endl;
		return 1;
	}

	int algorithm = atoi(argv[2]);

	switch(algorithm){

		case 1:
			_pascal_triangle();
			break;
		case 2:
			_coin_change();
			break;
		case 3:
		    _box_stacking();
			break;
		case 4:
			_dijkstra_shortest_path();
			break;
		case 5:
			_kruskal_mst();
            break;
        case 6:
            if(argc < 4){
                cout << "Parâmetros Insuficientes para o BPP_GA." << endl;
                cout << "1: numero_de_parametros" << endl;
                cout << "2: numero_do_algoritmo" << endl;
                cout << "3: numero_do_arquivo." << endl;
                return 1;
            }
            _bpp_ga(atoi(argv[3])); 
            
	}
    return 0;
}

