//
// Created by mz on 27/11/16.
//

#ifndef BPP_GA_BPP_GA_H
#define BPP_GA_BPP_GA_H

#include "Chromosome.hpp"
#include <fstream>

int firstFit(vector<int> weight, vector<int> iRandom, Chromosome &chromosome);
vector<int> readFile(string fileName);
vector<int> randomizeItemsSequence(vector<int> items);
int getLowerBound(vector<int> items);
Chromosome populate(vector<int> weight);


#endif //BPP_GA_BPP_GA_H
