#ifndef _DIJKSTRA_SHORTEST_PATH_
#define _DIJKSTRA_SHORTEST_PATH_

#include <stdio.h>
#include <limits.h>
  
// Number of vertices in the graph
#define VERTICES 6

// Finds the vertex with minimum distance value from
// the set of vertices not yet included in shortest path tree
int min_distance(int distance[], bool spt_set[]);

// Constructed distance array
void print_solution(int distance[], int num_vertices);

// Adjacency matrix representation
void dijkstra(int graph[VERTICES][VERTICES], int src);

#endif // _DIJKSTRA_SHORTEST_PATH_
