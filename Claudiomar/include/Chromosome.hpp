//
// Created by mz on 22/11/16.
//

#ifndef BPP_GA_CHROMOSOME_H
#define BPP_GA_CHROMOSOME_H

#include <vector>
#include "Bin.hpp"


using namespace std;

#define ACCEPTANCE_RATE 0.95

static const int WEIGHT_STORED = -1;

class Chromosome {

private:
    int totalWeight;
    vector<Bin> bins;
    int binCapacity;

public:

    Chromosome(int binCapacity);

    int getTotalWeight() const;

    int getNumberOfBins();

    void addWeight(int binIndex, int weightIndex, int weight);

    void printBins(vector<int> items, ofstream &out, bool writeToFile);

    Chromosome crossover(Chromosome &chr, vector<int> items);

    void addBinCrossOver(Bin b, Chromosome &newChr);

    void tryToAddBinCrossover(Bin b, int *crossWeightIndex, Chromosome &newChr);

    int firstFitCrossover(Chromosome &chromosome, vector<int> weight, int *crossWeightIndex);

};

#endif //BPP_GA_CHROMOSOME_H
