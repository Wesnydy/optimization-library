#ifndef _BOX_STACKING_H_
#define _BOX_STACKING_H_


#include <stdio.h>
#include <stdlib.h>

// Box structure in 3d
// For simplicity of solution, always keep l >= w 

struct Box {
	int h;
	int w;
	int l;
};

int compare(const void* a, const void* b);
int max (int a, int b);
int min (int a, int b);
int maxStackHeight(Box v[], int n);

#endif // _BOX_STACKING_H_
