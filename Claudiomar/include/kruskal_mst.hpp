#ifndef _KRUSKAL_MST_
#define _KRUSKAL_MST_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
// Weighted edge in graph
struct Edge {
    int source;
    int destination;
    int weight;
};
 
// Connected, undirected and weighted graph
struct Graph {
    int vertices; // Number of vertices
    int edges; // Number of edges
    struct Edge* edge; 
};
 
// Subset for union_sets-find_set
struct Subset {
    int parent;
    int rank;
};

// Creates a graph with vertices and edges
Graph* createGraph(int vertices, int edges);

// Find set of an element i
// Uses path compression technique
int find_set(Subset subsets[], int i);

// Union of two sets by rank
void union_sets(Subset subsets[], int x, int y);

// Compare two edges according to their weights.
// Used in qsort() for sorting an array of edges.
// http://www.cplusplus.com/reference/cstdlib/qsort/
int kcompare(const void* a, const void* b);

// Construct MST using Kruskal's algorithm
void kruskal_mst(Graph* graph);

#endif // _KRUSKAL_MST_
