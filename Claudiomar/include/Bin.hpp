//
// Created by mz on 23/11/16.
//

#ifndef BPP_GA_BIN_H
#define BPP_GA_BIN_H

#include <iostream>
#include <vector>

using namespace std;

class Bin {

private:
    int capacity;
    vector<int> weightsIndex; // indexes of the original vector of weights
    int totalWeight;

public:
    Bin(int capacity, int = 0);

    int getCapacity() const;

    void addWeight(int weightIndex, int weight);

    int getTotalWeight() const;

    void printWeightIndexes(vector<int> items, ofstream &outFile, bool writeToFile);

    bool compare(Bin b);

    void setCrossWeightIndex(int crossWeightIndex[]);

    bool wasCrossWeightIndexUsed(int crossWeightIndex[]);
};

#endif //BPP_GA_BIN_H
