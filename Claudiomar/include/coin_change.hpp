#ifndef _COIN_CHANGE_H_
#define _COIN_CHANGE_H_


#include <stdio.h>
#include <string.h>

// @param coins[] are the coins values, e.g, {1, 2, 3}
// @param value is the changing value, i.e, the total to be reached combining coins
// @return the number of ways to reach 'value' with 'coins'
int count_max(int coins[], int num_coins, int value);


#endif // _COIN_CHANGE_H_
