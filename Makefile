
CDIR:=$(CURDIR)/Claudiomar
JDIR:=$(CURDIR)/Jonathan
WDIR:=$(CURDIR)/Wesnydy

MAKEFILESDIR:="${CDIR}" "${JDIR}" "${WDIR}"

all: compile

compile:
	@ for i in ${MAKEFILESDIR}; do $(MAKE) -C "$$i" || exit; done

clean:
	@ for i in ${MAKEFILESDIR}; do $(MAKE) clean -C "$$i" || exit; done